import os
import sys
import yaml
import json
from oletools.olevba import VBA_Parser
import subprocess


class MacroFile:
    def __init__(self, name):
        self.filename = name

    def exists(self):
        if os.path.isfile(self.filename):
            return True
        else:
            return False

    def scan(self):
        '''Scans files with oletools.olevba. Returns:
        False: if file cannot be scanned
        score (integer): number of findings (excluded IOCs)'''

        result = {"file_scanned": False, "score": 0}

        try:
            vba = VBA_Parser(self.filename)
            result["file_scanned"] = True
        except:
            return result

        if vba.detect_vba_macros():
            data = vba.analyze_macros()
            vba.close()

            for type, keword, description in data:
                if type != 'IOC': # Do not count IOC type findings
                    result["score"] += 1
            return result
        else:
            # Scanned file with no macros found
            return result

    def quarantine(self, config):
        file = os.path.basename(self.filename)
        source = config["source_path"] + "/" + file
        target = config["quarantine_path"] + "/" + file
        try:
            os.rename(source, target)
        except:
            print("Error moving file")
            sys.exit(1)

    def move(self, config):
        file = os.path.basename(self.filename)
        source = config["source_path"] + "/" + file
        target = config["target_path"] + "/" + file
        try:
            os.rename(source, target)
        except:
            print("Error moving file")
            sys.exit(1)

    def delete(self, config):
        source = config["source_path"] + "/" + file
        try:
            os.unlink(source)
        except:
            print("Error removing file")
            sys.exit(1)

    def to_pdf(self, config):
        target = config["target_path"]
        outfile = os.path.basename(self.filename)
        subprocess.call(["/usr/bin/libreoffice", "--headless", "--convert-to",
            "pdf", "--outdir", target, outfile])

def get_configuration():
    config_file = 'config.yml'
    if os.path.isfile(config_file):
        with open(config_file, 'r') as cf:
            try:
                config = yaml.safe_load(cf)
                return config
            except:
                raise
                print('Error: could not read configuration')
                return False
    else:
        print('Error: {} not found'.format(config_file))
        return False


def main(filename):
    # Import YAML configuration
    config = {}
    yaml_config = get_configuration()
    if config == False:
        sys.exit(1)

    config["source_path"] = yaml_config["path"]["in"]
    config["target_path"] = yaml_config["path"]["out"]
    config["quarantine_path"] = yaml_config["path"]["quarantine"]
    config["action_on_clean"] = yaml_config["action"]["on_clean"]
    config["action_on_infected"] = yaml_config["action"]["on_infected"]


    # Scan file
    macro_file = MacroFile(filename)

    if macro_file.exists():
        result = macro_file.scan()
    else:
        print("Error: file not found: {}".format(filename))
        sys.exit(1)

    if result["file_scanned"] == False:
        print("Unsupported filetype: {}".format(filename))
        sys.exit(1)
    else:
        if result["score"] > 0:
            if config["action_on_infected"] == "quarantine":
                macro_file.quarantine(config)
            elif config["action_on_infected"] == "delete":
                macro_file.delete(config)
            elif config["action_on_infected"] == "to_pdf":
                macro_file.to_pdf(config)
            else:
                print("Error: action not implemented")
                sys.exit(1)

        else:
            print("The file is clean: {}".format(os.path.basename(filename)))
            if config["action_on_clean"] == "to_pdf":
                macro_file.to_pdf(config)
            else:
                macro_file.move(config)


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("\n  Usage: {} filename\n".format(sys.argv[0]))
        sys.exit(1)
    main(sys.argv[1])
